import requests
import re
from datetime import datetime
import time
from scipy import stats
import numpy as np
import math

# For timing execution
start_time = time.time()

# ----------------------- Elasticsearch Methods -----------------------------
def searchForMethodName(name):
	query_body = '{"query":{"match":{"message":"[M:%s]"}}}' % name
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForThreadId(thread_id, file_path):
	query_body = '{"query":{"bool":{"should":[{"match":{"message":"[%s]"}},{"match":{"path":"%s"}}]}}}' % (thread_id, file_path)
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()
# --------------------------- Helper Methods --------------------------------
def get_date(record):
	return datetime.strptime(str(record[0] + " " + record[1]), "%Y-%m-%d %H:%M:%S,%f")

# ---------------------------------------------------------------------------

# Get all data for that method name
method_to_search = "methodToSearch"
print("Querying for method: %s" % method_to_search)
response_data_method = searchForMethodName(method_to_search)

# Extract log messages into a list
found_occurrences = 0
log_messages = []
log_file = []
for result in response_data_method['hits']['hits']:
	log_messages.append(result['_source']['message'])
	log_file.append(result['_source']['path'])

# Parse log messages
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"
matched = []
matched_file = []

found_occurrences = len(log_messages)
for i in range(0, found_occurrences-1):
	res = re.findall(pattern, log_messages[i])
	if res:
		matched.append(res)
		matched_file.append(log_file[i])

# For every method in results set find all entries for that thread ID containing the method
result_dict = {}

print("Searching for %d thread ID's" % len(log_messages))
for i in range(0, len(matched)-1):

	thread_entry = matched[i]
	matched_loc = matched_file[i]

	if len(thread_entry) >0 :

		entry_tuple = thread_entry[0]

		search_id = entry_tuple[0] + "_" + entry_tuple[1] + "_" + entry_tuple[2].strip() + "_" + entry_tuple[5]
		thread_id = entry_tuple[2]

		# Execute search for this thread ID
		response_data_method = searchForThreadId(thread_id, matched_loc)

		# Parse log results
		method_found = False

		log_messages = []
		for result in response_data_method['hits']['hits']:
			log_messages.append(result['_source']['message'])

		matched_new = [re.findall(pattern, entry) for entry in log_messages]

		print("Search " + str(i) + " Results:" + str(len(matched_new)))
		if len(matched_new) > 0:
			times = []
			current_results = []
			for thread_entry_matched in matched_new:
				if len(thread_entry_matched) > 0 and thread_entry_matched[0][2] == thread_id:
					current_results.append(thread_entry_matched[0])
					times.append((entry_tuple[0], entry_tuple[1]))

					if entry_tuple[5] in thread_entry_matched[0]:
						method_found = True

		if method_found:
			# Sort current_results and add to dictionary
			result_dict.update({search_id: sorted(current_results, key=get_date, reverse=False)})

# Get all time differences between methods
times = []
for key, value in result_dict.items():
	previous_key = None
	previous_entry = None

	for v in value:
		if previous_key is None and previous_entry is None:
			previous_key = key
			previous_entry = v

		temp = get_date(v) - get_date(previous_entry)
		times.append(math.ceil(temp.total_seconds()))

print("\nTime Distribution Stats:\n" + str(stats.describe(np.array(times))))

print("\nTime mode:\n" + str(stats.mode(np.array(times))[0][0]))

print("\nPercentile Range (0% - 90%):\n" + str([np.percentile(np.array(times), centile) for centile in range(0,100,10)]))

print("\n\n--- %s seconds ---" % (time.time() - start_time))