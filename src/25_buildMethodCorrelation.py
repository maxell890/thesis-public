import requests
import re
from datetime import datetime
from time import time

# For timing execution
start_time = time()

# ----------------------- Elasticsearch Methods -----------------------------
def searchForMethodName(name):
	query_body = '{"query":{"match":{"message":"[M:%s]"}}}' % name
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=1000'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForThreadId(thread_id, file_path):
	query_body = '{"query":{"bool":{"should":[{"match":{"message":"[%s]"}},{"match":{"path":"%s"}}]}}}' % (thread_id, file_path)
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForTimeStamp(timeStamp):
	query_body = '{"query":{"match":{"message":"%s"}}}' % timeStamp
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

# --------------------------- Helper Methods --------------------------------
def get_date(record):
	return datetime.strptime(str(record[0] + " " + record[1]), "%Y-%m-%d %H:%M:%S,%f")

# ---------------------------------------------------------------------------

# Get all data for that method name
method_to_search = "methodToSearch"
print("Querying for method: %s" % method_to_search)
response_data_method = searchForMethodName(method_to_search)

# Extract log messages into a list
found_occurrences = 0
log_messages = []
log_file = []
for result in response_data_method['hits']['hits']:
	log_messages.append(result['_source']['message'])
	log_file.append(result['_source']['path'])

# Parse log messages
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"
matched = []
matched_file = []

found_occurrences = len(log_messages)
for i in range(0, found_occurrences-1):
	res = re.findall(pattern, log_messages[i])
	if res:
		matched.append(res)
		matched_file.append(log_file[i])

# For every method in results set find all entries for that thread ID containing the method
result_dict = {}

print("Searching for %d time stamps" % found_occurrences)
for i in range(0, len(matched)-1):

	thread_entry = matched[i]
	matched_loc = matched_file[i]
	if len(thread_entry) >0 :

		entry_tuple = thread_entry[0]

		search_id = entry_tuple[0] + "_" + entry_tuple[1] + "_" + entry_tuple[2].strip() + "_" + entry_tuple[5]
		timeStamp_id = entry_tuple[0] + " " + entry_tuple[1]

		# Execute search for timeStamp
		response_data_method = searchForTimeStamp(timeStamp_id)

		# Parse log results
		same_log_file = False

		log_messages = []
		log_file = []
		for result in response_data_method['hits']['hits']:
			log_messages.append(result['_source']['message'])
			log_file.append(result['_source']['path'])

		for i in range(0, len(log_messages) - 1):
			res = re.findall(pattern, log_messages[i])
			if res and len(res[0]) > 6 and matched_loc != log_file[i]:

				# time diff between methods
				temp = get_date(entry_tuple) - get_date(res[0])
				if abs(int(temp.total_seconds())) <= 1:
					current = result_dict.get(log_file[i], [])
					current.append(res[0][5])
					result_dict.update({log_file[i]: current})

for key, value in result_dict.items():
	occurances_in_file = []
	methods_in_file = set(value)
	for method in methods_in_file:
		count = value.count(method)
		occurances_in_file.append((method, count))
	occurances_in_file.sort(key=lambda tup: tup[1], reverse=True)

	if len(occurances_in_file) >= 1:
		print("\n" + key)
		for i in range(0, 5):
			if len(occurances_in_file)-1 >= i:
				found = occurances_in_file[i][1]
				distance = (found/found_occurrences)-1
				print(str(occurances_in_file[i][0]) + ": " + str(occurances_in_file[i][1]) + "/" + str(found_occurrences) + " Distance: " + str(abs(distance)))

#-----------------------------------------------------------------------------------------

print("\n\n--- %s seconds ---" % (time() - start_time))
