# (\\d{4}-\\d{2}-\\d{2}) (\\d{2}:\\d{2}:\\d{2},\\d{3}) \\[(.*)\\] ([^ ]*) +([^ ]*) - (.*)$
# p = re.compile("""(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - (.*)$""", re.VERBOSE)
# pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - (.*)$"
# (\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$

import os
import re

path = 'C:\\Users\\Sean\\Documents\\Projects\\Git\\nuig\\Thesis\\data\\'
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"

for filename in os.listdir(path):

	if filename.endswith(".log"):
		file = open(path + filename, "r", encoding="utf8")

		print("\n\n\n/*/*/*/*/*/*/*/*/*/*/*/*/*/*/" + filename + "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/\n\n\n")
		for line in file:
			matched = re.findall(pattern, line)
			if matched:
				print(matched)
