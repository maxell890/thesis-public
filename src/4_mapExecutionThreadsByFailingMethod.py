import os
import re
import time

# Stats Variables
start_time = time.time()
logLineCount = 0
# Enf of stats variables

path = 'C:\\Users\\Sean\\Documents\\Projects\\Git\\nuig\\Thesis\\data\\'
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"



for filename in os.listdir(path):

	if filename.endswith(".log"):
		file = open(path + filename, "r", encoding="utf8")

		print("\n\n\n/*/*/*/*/*/*/*/*/*/*/*/*/*/*/" + filename + "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/\n\n\n")
		errroDictionary = {}
		threadId = ""
		threadExecutions = []

		for line in file:
			logLineCount += 1
			matched = re.findall(pattern, line)

			if matched:
				threadId = matched[0][2]
				methodName = matched[0][5]
				logType = matched[0][3]

				if threadId == threadId:
					threadExecutions.append(methodName)
				else:
					#print(threadExecutions)
					threadExecutions = []
					threadId = threadId
					threadExecutions.append(threadId + "==> ")
					threadExecutions.append(threadId)

				if logType == 'ERROR':
					if methodName in errroDictionary:
						currentErrorPaths = errroDictionary.get(methodName)
						currentErrorPaths.append(threadExecutions)
						errroDictionary.update({methodName: currentErrorPaths})
					else:
						errroDictionary.update({methodName: [threadExecutions]})

		for k in errroDictionary:
			print('Method: ' + k)
			methodResults = errroDictionary.get(k)
			print('Size: ' + str(len(methodResults)))
			# for entry in methodResults:
			# 	print('\t' + str(entry))
			# print('\n')

print("\n\n--- %s seconds ---" % (time.time() - start_time))
print("-- %s log lines --" % logLineCount)
