import os
import re

path = 'C:\\Users\\Sean\\Documents\\Projects\\Git\\nuig\\Thesis\\data\\'
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"

for filename in os.listdir(path):

	if filename.endswith(".log"):
		file = open(path + filename, "r", encoding="utf8")

		print("\n\n\n/*/*/*/*/*/*/*/*/*/*/*/*/*/*/" + filename + "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/\n\n\n")
		threadId = ""
		threadExecutions = []

		for line in file:
			matched = re.findall(pattern, line)
			if matched:
				if threadId == matched[0][2]:
					threadExecutions.append(matched[0][5])
				else:
					print(threadExecutions)
					threadExecutions = []
					threadId = matched[0][2]
					threadExecutions.append(threadId + "==> ")
					threadExecutions.append(matched[0][2])
