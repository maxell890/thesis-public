import requests
import re
from datetime import datetime
from time import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

# For timing execution
start_time = time()

# ----------------------- Elasticsearch Methods -----------------------------
def searchForMethodName(name):
	query_body = '{"query":{"match":{"message":"[M:%s]"}}}' % name
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=1000'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForThreadId(thread_id, file_path):
	query_body = '{"query":{"bool":{"should":[{"match":{"message":"[%s]"}},{"match":{"path":"%s"}}]}}}' % (thread_id, file_path)
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForTimeStamp(timeStamp):
	query_body = '{"query":{"match":{"message":"%s"}}}' % timeStamp
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()
# --------------------------- Helper Methods --------------------------------
def get_date(record):
	return datetime.strptime(str(record[0] + " " + record[1]), "%Y-%m-%d %H:%M:%S,%f")

# ---------------------------------------------------------------------------

# Get all data for that method name
method_to_search = "methodToSearch"
print("Querying for method: %s" % method_to_search)
response_data_method = searchForMethodName(method_to_search)

# Extract log messages into a list
log_messages = []
log_file = []
for result in response_data_method['hits']['hits']:
	log_messages.append(result['_source']['message'])
	log_file.append(result['_source']['path'])

# Parse log messages
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"
matched = []
matched_file = []

for i in range(0, len(log_messages)-1):
	res = re.findall(pattern, log_messages[i])
	if res:
		matched.append(res)
		matched_file.append(log_file[i])

# For every method in results set find all entries for that thread ID containing the method
result_list_message = []
result_list_file = []

print("Searching for %d time stamps" % len(log_messages))
for i in range(0, len(matched)-1):

	thread_entry = matched[i]
	matched_loc = matched_file[i]
	if len(thread_entry) >0 :

		entry_tuple = thread_entry[0]

		search_id = entry_tuple[0] + "_" + entry_tuple[1] + "_" + entry_tuple[2].strip() + "_" + entry_tuple[5]
		timeStamp_id = entry_tuple[0] + " " + entry_tuple[1]

		# Execute search for timeStamp
		response_data_method = searchForTimeStamp(timeStamp_id)

		# Parse log results
		same_log_file = False

		log_messages = []
		log_file = []
		for result in response_data_method['hits']['hits']:
			log_messages.append(result['_source']['message'])
			log_file.append(result['_source']['path'])

		for i in range(0, len(log_messages) - 1):
			res = re.findall(pattern, log_messages[i])
			if res and len(res[0]) > 6 and matched_loc != log_file[i]:
				result_list_message.append(res[0][7])
				result_list_file.append(log_file[i])

corpus = result_list_message
corpus_label = result_list_file

unique_file_paths = set(corpus_label)
print(unique_file_paths)

#-----------------------------------------------------------------------------------------
n_samples = 2100
n_features = 800

n_topics = 6
n_topWords = 15

def print_top_words(model, feature_names, n_topWords):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_topWords - 1:-1]]))
    print()

print("Extracting tf features for LDA...")
tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2, max_features=n_features, stop_words='english')

t0 = time()
tf = tf_vectorizer.fit_transform(corpus)
print("done in %0.3fs." % (time() - t0), "\n")

print("Fitting LDA models with tf features, ", "n_samples=%d and n_features=%d..." % (n_samples, n_features))
lda = LatentDirichletAllocation(n_components=n_topics, max_iter=5, learning_method='online', learning_offset=50., random_state=0)

t0 = time()
lda.fit(tf)
doc_topic_distrib = lda.transform(tf)
print("done in %0.3fs." % (time() - t0), "\n")

print("\nTopics in LDA model:")
tf_feature_names = tf_vectorizer.get_feature_names()

tf_feature_names = tf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, n_topWords)


print("\n\n--- %s seconds ---" % (time() - start_time))
