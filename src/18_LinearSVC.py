import requests
import re
from datetime import datetime
import time
from sklearn import svm
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import ShuffleSplit

# For timing execution
start_time = time.time()

# ----------------------- Elasticsearch Methods -----------------------------
def searchForMethodName(name):
	query_body = '{"query":{"match":{"message":"[M:%s]"}}}' % name
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForThreadId(thread_id, file_path):
	query_body = '{"query":{"bool":{"should":[{"match":{"message":"[%s]"}},{"match":{"path":"%s"}}]}}}' % (thread_id, file_path)
	query_url = 'http://localhost:9200/loganalyticsfull/_search?size=200'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()
# --------------------------- Helper Methods --------------------------------
def get_date(record):
	return datetime.strptime(str(record[0] + " " + record[1]), "%Y-%m-%d %H:%M:%S,%f")

# ---------------------------------------------------------------------------

# Get all data for that method name
method_to_search = "methodToSearch"
print("Querying for method: %s" % method_to_search)
response_data_method = searchForMethodName(method_to_search)

# Extract log messages into a list
found_occurrences = 0
log_messages = []
log_file = []
for result in response_data_method['hits']['hits']:
	log_messages.append(result['_source']['message'])
	log_file.append(result['_source']['path'])

# Parse log messages
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"
matched = []
matched_file = []

found_occurrences = len(log_messages)
for i in range(0, found_occurrences-1):
	res = re.findall(pattern, log_messages[i])
	if res:
		matched.append(res)
		matched_file.append(log_file[i])

# For every method in results set find all entries for that thread ID containing the method
result_dict = {}

print("Searching for %d thread ID's" % len(log_messages))
for i in range(0, len(matched)-1):

	thread_entry = matched[i]
	matched_loc = matched_file[i]

	if len(thread_entry) >0 :

		entry_tuple = thread_entry[0]

		search_id = entry_tuple[0] + "_" + entry_tuple[1] + "_" + entry_tuple[2].strip() + "_" + entry_tuple[5]
		thread_id = entry_tuple[2]

		# Execute search for this thread ID
		response_data_method = searchForThreadId(thread_id, matched_loc)

		# Parse log results
		method_found = False

		log_messages = []
		for result in response_data_method['hits']['hits']:
			log_messages.append(result['_source']['message'])

		matched_new = [re.findall(pattern, entry) for entry in log_messages]

		print("Search " + str(i) + " Results:" + str(len(matched_new)))
		if len(matched_new) > 0:
			times = []
			current_results = []
			for thread_entry_matched in matched_new:
				if len(thread_entry_matched) > 0 and thread_entry_matched[0][2] == thread_id:
					current_results.append(thread_entry_matched[0])
					times.append((entry_tuple[0], entry_tuple[1]))

					if entry_tuple[5] in thread_entry_matched[0]:
						method_found = True

		if method_found:
			# Sort current_results and add to dictionary
			result_dict.update({search_id: sorted(current_results, key=get_date, reverse=False)})

# for all results create journeys. This is based on the time between methods.
# Anything greater than 2 seconds is probably a separate journey
all_journeys = []
for key, value in result_dict.items():
	previous_key = None
	previous_entry = None
	journey = []

	for v in value:

		if previous_key is None and previous_entry is None:
			previous_key = key
			previous_entry = v
			journey.append(v)

		temp = get_date(v) - get_date(previous_entry)

		if int(temp.total_seconds()) < 2:
			journey.append(v)
		else:
			if len(journey) > 1:
				all_journeys.append(journey)
			journey = []
			journey.append(v)

		previous_key = key
		previous_entry = v

# Get all method transitions
print("Computing method transitions")
method_flow_list = []
method_error_path_list = []
for journey in all_journeys:
	journey_methods = []
	journey_contains_error = False
	for step in journey:
		journey_methods.append(step[5])

		if step[3] == 'ERROR':
			journey_contains_error = True
			#break

	method_flow_list.append(' '.join(journey_methods))
	if journey_contains_error:
		method_error_path_list.append('ERROR')
	else:
		method_error_path_list.append('NOT_ERROR')

# Create corpus
print("Building corpus for training")
corpus = []
corpus_label = []

corpus = method_flow_list
corpus_label = method_error_path_list

print("Corpus Size: %d" % len(corpus))
print(" -ERROR message count: %d" % corpus_label.count("ERROR"))
print(" -NOT_ERROR message count: %d" % corpus_label.count("NOT_ERROR"))

# Build Classifier
print("Building Pipeline")
text_clf = Pipeline([('vect', CountVectorizer()), ('clf', svm.LinearSVC())])

# Split and cross validate
print("Cross Validating")
shuffle_split = ShuffleSplit(n_splits=3, test_size=0.3, random_state=0)
scores = cross_val_score(text_clf, corpus, corpus_label, cv=shuffle_split)
print("\nAccuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

print("\n\n--- %s seconds ---" % (time.time() - start_time))
