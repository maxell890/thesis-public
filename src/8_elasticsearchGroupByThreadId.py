import requests
import re

# ----------------------- Elasticsearch Methods -----------------------------
def searchForMethodName(name):
	query_body = '{"query":{"match":{"message":"[M:%s]"}}}' % name
	query_url = 'http://localhost:9200/loganalytics/_search?size=1000'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()

def searchForThreadId(thread_id):
	query_body = '{"query":{"match":{"message":"[%s]"}}}' % thread_id
	query_url = 'http://localhost:9200/loganalytics/_search?size=1000'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(query_url, headers=headers, data=query_body)
	return r.json()
# ---------------------------------------------------------------------------

# Get all data for that method name
response_data = searchForMethodName("methodToSearch")

# Extract log messages into a list
log_messages = []
for result in response_data['hits']['hits']:
	log_messages.append(result['_source']['message'])

# Parse log messages
pattern = "(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2},\d{3}) \[(.*?)\] ([^ ]*) +([^ ]*) - \[(.*?)\]\[(.*?)\]. (.*)$"
matched = [re.findall(pattern, entry) for entry in log_messages]

# Group executions by thread id
grouped_executions_dict = {}

for entry in matched:
	entry_tuple = entry[0]
	thread_name = entry_tuple[2]

	if thread_name in grouped_executions_dict:
		current_paths = grouped_executions_dict.get(thread_name)
		current_paths.append(entry_tuple)
		grouped_executions_dict.update({thread_name: current_paths})
	else:
		grouped_executions_dict.update({thread_name: [entry_tuple]})

# --------------- Print Data ---------------------
for keys, values in grouped_executions_dict.items():
	print(keys)
	print('%d \n' % len(values))
